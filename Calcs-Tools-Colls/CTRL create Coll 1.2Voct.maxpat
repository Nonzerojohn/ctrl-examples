{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 4,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 312.0, 105.0, 742.0, 700.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 474.0, 389.0, 150.0, 20.0 ],
					"style" : "",
					"text" : "1.2V/Octave version!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 230.0, 572.0, 267.0, 33.0 ],
					"style" : "",
					"text" : "<< data saved with patcher, check inspector properties to switch off if required."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 459.0, 281.0, 150.0, 60.0 ],
					"style" : "",
					"text" : "creates a Coll for CTRL\n\nNeeds some work in terms of initialisation!"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 348.0, 204.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 51,
						"data" : [ 							{
								"key" : 24,
								"value" : [ 0 ]
							}
, 							{
								"key" : 25,
								"value" : [ 1311 ]
							}
, 							{
								"key" : 26,
								"value" : [ 2621 ]
							}
, 							{
								"key" : 27,
								"value" : [ 3932 ]
							}
, 							{
								"key" : 28,
								"value" : [ 5243 ]
							}
, 							{
								"key" : 29,
								"value" : [ 6554 ]
							}
, 							{
								"key" : 30,
								"value" : [ 7864 ]
							}
, 							{
								"key" : 31,
								"value" : [ 9175 ]
							}
, 							{
								"key" : 32,
								"value" : [ 10486 ]
							}
, 							{
								"key" : 33,
								"value" : [ 11796 ]
							}
, 							{
								"key" : 34,
								"value" : [ 13107 ]
							}
, 							{
								"key" : 35,
								"value" : [ 14418 ]
							}
, 							{
								"key" : 36,
								"value" : [ 15728 ]
							}
, 							{
								"key" : 37,
								"value" : [ 17039 ]
							}
, 							{
								"key" : 38,
								"value" : [ 18350 ]
							}
, 							{
								"key" : 39,
								"value" : [ 19661 ]
							}
, 							{
								"key" : 40,
								"value" : [ 20971 ]
							}
, 							{
								"key" : 41,
								"value" : [ 22282 ]
							}
, 							{
								"key" : 42,
								"value" : [ 23593 ]
							}
, 							{
								"key" : 43,
								"value" : [ 24903 ]
							}
, 							{
								"key" : 44,
								"value" : [ 26214 ]
							}
, 							{
								"key" : 45,
								"value" : [ 27525 ]
							}
, 							{
								"key" : 46,
								"value" : [ 28835 ]
							}
, 							{
								"key" : 47,
								"value" : [ 30146 ]
							}
, 							{
								"key" : 48,
								"value" : [ 31457 ]
							}
, 							{
								"key" : 49,
								"value" : [ 32768 ]
							}
, 							{
								"key" : 50,
								"value" : [ 34078 ]
							}
, 							{
								"key" : 51,
								"value" : [ 35389 ]
							}
, 							{
								"key" : 52,
								"value" : [ 36700 ]
							}
, 							{
								"key" : 53,
								"value" : [ 38010 ]
							}
, 							{
								"key" : 54,
								"value" : [ 39321 ]
							}
, 							{
								"key" : 55,
								"value" : [ 40632 ]
							}
, 							{
								"key" : 56,
								"value" : [ 41942 ]
							}
, 							{
								"key" : 57,
								"value" : [ 43253 ]
							}
, 							{
								"key" : 58,
								"value" : [ 44564 ]
							}
, 							{
								"key" : 59,
								"value" : [ 45874 ]
							}
, 							{
								"key" : 60,
								"value" : [ 47185 ]
							}
, 							{
								"key" : 61,
								"value" : [ 48496 ]
							}
, 							{
								"key" : 62,
								"value" : [ 49807 ]
							}
, 							{
								"key" : 63,
								"value" : [ 51117 ]
							}
, 							{
								"key" : 64,
								"value" : [ 52428 ]
							}
, 							{
								"key" : 65,
								"value" : [ 53739 ]
							}
, 							{
								"key" : 66,
								"value" : [ 55049 ]
							}
, 							{
								"key" : 67,
								"value" : [ 56360 ]
							}
, 							{
								"key" : 68,
								"value" : [ 57671 ]
							}
, 							{
								"key" : 69,
								"value" : [ 58981 ]
							}
, 							{
								"key" : 70,
								"value" : [ 60292 ]
							}
, 							{
								"key" : 71,
								"value" : [ 61603 ]
							}
, 							{
								"key" : 72,
								"value" : [ 62914 ]
							}
, 							{
								"key" : 73,
								"value" : [ 64224 ]
							}
, 							{
								"key" : 74,
								"value" : [ 65535 ]
							}
 ]
					}
,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 81.5, 563.0, 114.0, 22.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1
					}
,
					"style" : "",
					"text" : "coll CTRL-1.2V-Oct"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 106.0, 204.0, 122.0, 22.0 ],
					"style" : "",
					"text" : "t b i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 326.0, 462.0, 87.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 287.0, 427.0, 33.0, 22.0 ],
					"style" : "",
					"text" : "int 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 287.0, 393.0, 38.0, 22.0 ],
					"style" : "",
					"text" : "+ 0.5"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-4",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 287.0, 362.0, 79.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 81.5, 517.0, 163.0, 22.0 ],
					"style" : "",
					"text" : "74 65535"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 138.0, 462.0, 168.0, 22.0 ],
					"style" : "",
					"text" : "pack 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 138.0, 242.0, 50.0, 22.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 204.0, 82.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 204.0, 123.0, 71.0, 22.0 ],
					"style" : "",
					"text" : "metro 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 261.0, 279.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "1310.7"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 215.0, 279.0, 29.5, 22.0 ],
					"style" : "",
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 215.0, 242.0, 65.0, 22.0 ],
					"style" : "",
					"text" : "sel 24"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 204.0, 165.0, 235.0, 22.0 ],
					"style" : "",
					"text" : "counter 24 74"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 341.5, 268.0, 47.0, 22.0 ],
					"style" : "",
					"text" : "float 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 287.0, 312.0, 31.0, 22.0 ],
					"style" : "",
					"text" : "+ 0."
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"midpoints" : [ 296.5, 351.0, 412.0, 351.0, 412.0, 252.0, 351.0, 252.0 ],
					"order" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"order" : 1,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"midpoints" : [ 357.5, 237.0, 459.0, 237.0, 459.0, 54.0, 213.5, 54.0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 1 ],
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 1 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-3", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 1 ],
					"order" : 1,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-9", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-9", 2 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0
	}

}
