# CTRL #

This repository houses example code for CTRL

CTRL is a Eurorack CV/Gate interface that is cross platform (Win, Liux, OSX) 
and can even work with embedded technologies like Raspberry Pi, or Android TV Boxes (running Armbian and Pd)

It leverages standard USB Serial communication functionality.