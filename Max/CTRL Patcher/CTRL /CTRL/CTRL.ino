 //----------------------------------------------------------------------------------------------------//
 //----------------------------------------Max/Pd to CTRL----------------------------------------------//

 //------------------------------beta code John Harding, 2017------------------------------------------//
 
 //----------------------------------- streamlined code: Edwin Park, 201------------------------------//

 // Proposed data format concept... 17 bytes wide minimum. The USB packet size is 64 bytes. Maybe packet structure usage could be more efficient?
 // Arduino cookbook states "Messages can contain one or more special characters that identify the start of the message�this is called the header."
 // For an example see here: example 4.8: https://www.safaribooksonline.com/library/view/arduino-cookbook/9781449399368/ch04.html
 
 // [header] '/' character (ascii 47) for message start identification
 // [lowByte0][highByte0] //DAC channel 0
 // [lowByte1][highByte1] //DAC channel 1
 // [lowByte2][highByte2] //DAC channel 2
 // [lowByte3][highByte3] //DAC channel 3
 // [lowByte4][highByte4] //DAC channel 4
 // [lowByte5][highByte5] //DAC channel 5
 // [lowByte6][highByte6] //DAC channel 6
 // [lowByte7][highByte7] //DAC channel 7
 // [gateByte] //Gates array: 0 off, 1 on, eight gates used (one binary byte wide).
 
 // Note: serial object in Max allows you to set @chunk [int] (data list length) //also look at DTR (data terminal ready) function. Check Pd equivalent.
 
 //-------------------------------------HARDWARE NOTES---------------------------------------------//
 
 /*  Pin headers, looking from DAC side of main PCB
 
 // DAC OUT HEADER (BTM):
 
 | 6 | 4 | 2 | 0 | 1 | 3 | 5 | 7 |
 
 // GATE HEADER (TOP):
 
   | D+ | D- | GND | GND | GND | GND | GND | GND | GND | GND |
   | D+ | D- | D04 | D02 | D03 | D05 | D06 | D08 | D12 | D09 |
 
 */
 //-----------------------------------------------------------------------------------------------//
 
 //------------------------------------Includes---------------------------------------------------//
 #include <DA8568C.h>
 //----------------------------------------------------------------------------------------------//
 
 //-------------------------------------Constructor----------------------------------------------//
 DA8568C dac; // this implies using the PINs as defined in the Library (modified to match CTRL)
 // otherwise use constructor arguments like
 // DA8568C(int dataoutpin=3, int spiclkpin=4, int slaveselectpin=5, int ldacpin=6, int clrpin=7);
 //-----------------------------------------------------------------------------------------------//
 
 //-------------------------------------Constants-------------------------------------------------//
 
 #define NUM_GATES 8
 #define NUM_DACS 8
 
 const int gatePin[] = {9, 12, 8, 6, 4, 2, 3, 5};
 
 //-----------------------------------------------------------------------------------------------//
 
//-------------------------------------Variables-------------------------------------------------//

uint16_t dacVals[NUM_DACS];
boolean gateVals[NUM_GATES];

//-----------------------------------------------------------------------------------------------//

//-------------------------------------Main Program----------------------------------------------//
 void setup() {
   dac.init(); //initialise the DAC
   initGates(); //initialise gate pins
   Serial.begin(115200); // 115200 is the default
 }
 
 void loop() {
   if (Serial.available()) {
     char c = Serial.read();
     if (c == '/') {
       for (int i=NUM_DACS-1; i >= 0; i--) {
         uint16_t dacVal = readDAC();
         // Serial.print("DAC");
         // Serial.print(i);
         // Serial.print(": ");
         // Serial.println(dacVal);
         writeDAC(i, dacVal);
       }
 
       //read the gate byte at the end of the chain of bytes
       uint8_t gates = readGates();
       // Serial.print("gates: ");
       // Serial.println(gates);
       trigGates(gates);
     }
   }
 }
 
 //----------------------------------------Functions--------------------------------------//
 
 void initGates() {
   for (int i = 0; i < NUM_GATES; i++) {
     pinMode(gatePin[i], OUTPUT);
     digitalWrite(gatePin[i], LOW);
   }
 }
 
 uint8_t readGates() {
   while (true) {
     if (Serial.available()) {
       return Serial.read();
     }
   }
 }
 
 void trigGates(uint8_t gates) {
   for (int i = 0; i < NUM_GATES; i++) {

    boolean gateVal = gates & (0x01 << i);
    if (gateVal != gateVals[i]) {
      digitalWriteFast(gatePin[i], gateVal);
      gateVals[i] = gateVal;
    }
   }
 }
 
 uint16_t readDAC() {
   uint8_t bytes[2];
 
   int byte_count = 0;
   while (byte_count < 2) {
     if (Serial.available()) {
       bytes[byte_count++] = Serial.read();
     }
   }
 
   return (bytes[1] << 8) + bytes[0];
 }
 
 void writeDAC(int i, uint16_t dacVal) {
  if (dacVal != dacVals[i]) {
    dac.write(WRITE_UPDATE_N, i, dacVal);
    dacVals[i] = dacVal;
  }
 }
